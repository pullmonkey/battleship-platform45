Battleship::Application.routes.draw do
  get "game/new", :as => 'new_game'

  match "game/play", :as => 'play_game'

  get "game/update_grid", :as => 'update_grid'

  root :to => "game#index"
end
