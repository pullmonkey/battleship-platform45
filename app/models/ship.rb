class Ship < ActiveRecord::Base
  attr_accessible :game_id, :name, :length
  belongs_to :game

  scope :alive, where(:sunk => false)
  scope :sunk, where(:sunk => true)

  def sink!
    update_attribute(:sunk, true)
  end
end
