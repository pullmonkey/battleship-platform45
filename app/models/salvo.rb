class Salvo < ActiveRecord::Base
  attr_accessible :error, :game_id, :status, :x, :y, :enemy, :hit, :sunk
  belongs_to :game

  after_create :sink_related

  scope :success, where(:hit => true)
  scope :enemy, where(:enemy => true)
  scope :ours, where(:enemy => false)
  scope :sunk, where("sunk is not null")
  scope :not_sunk, where("sunk is null")
  scope :missed, where(:status => "miss")
  scope :hit, where(:status => "hit")
  scope :non_sunk_hit, hit.not_sunk  # hits to focus on when determining next nuke coords

  def self.next_best_shot
    if non_sunk_hit.count > 0 and (best_selection = (all_surrounding_coords & available_coords)).any?
      # TODO consider shots following a path, for example, dont hunt in the y direction ...
      # TODO ... when we have two consecutive hits in the y direction
      # let's knock out ships that we have hits on
      return best_selection.first
    else
      # ok, guess we are going to pick a random shot out of available grid spaces
      return available_coords.sample
    end
  end

  # the coords surrounding our hits (non sunk) 
  def self.all_surrounding_coords
    ours.non_sunk_hit.map(&:surrounding_coords).flatten(1)
  end

  def self.available_coords
    all_coords = coords_for_grid(10,10)
    all_coords - ours.map(&:coords)
  end

  def self.fire(x,y,status,sunk=nil)
    failed_nuke(x,y,status) if status == "miss"
    successful_nuke(x,y,status,sunk) if status == "hit"
  end
  
  def self.received_enemy_fire(x,y)
    create(:x => x, :y => y, :enemy => true)
  end

  def coords
    [x,y]
  end

  # the four surrounding points (may be out of bounds)
  def surrounding_coords
    [[x+1,y], [x-1,y], [x,y+1], [x,y-1]]
  end

  private 

  # salvos consecutive along the same access as this salvo and that have an appropriate count
  def related_salvos(shots_needed)
    logger.debug "Looking for other shots to 'sink'"
    # find x and y salvos to compare with
    xs = game.salvos.ours.where(:x => x, :y => (y-shots_needed..y+shots_needed))
    ys = game.salvos.ours.where(:y => y, :x => (x-shots_needed..x+shots_needed))
    logger.debug "x direction 1: #{xs.inspect}"
    logger.debug "y direction 1: #{ys.inspect}"
    # look into the salvos and find the consecutive ones for each direction
    consecutive_xs = ys.map(&:x).sort.groupulate.find{|xss| xss.include?(x)}
    consecutive_ys = xs.map(&:y).sort.groupulate.find{|yss| yss.include?(y)}
    logger.debug "consecutive_x direction: #{consecutive_xs.inspect}"
    logger.debug "consecutive_y direction: #{consecutive_ys.inspect}"
    # update x's and y's based on the consecutive selection
    xs = game.salvos.ours.where(:x => x, :y => consecutive_ys)
    ys = game.salvos.ours.where(:y => y, :x => consecutive_xs)
    logger.debug "x direction 2: #{xs.inspect}"
    logger.debug "y direction 2: #{ys.inspect}"
    
    # figure out which salvos to focus on
    salvos = xs.count >= shots_needed ? xs : ys
    logger.debug "Determined salvos to be #{salvos.inspect}"
    salvos
  end

  # sink related shots so that we can ignore them when finding best coord to play
  def sink_related
    unless sunk.blank?
      # update :sunk for all shots associated with this shot
      shots_needed = game.ships.find_by_name(sunk).length
      logger.debug "need #{shots_needed} shots to sink the #{sunk}"
      # only need to look for others if we have shots needing updating
      if shots_needed > 1
        salvos = related_salvos(shots_needed)
        salvos.each{|s| s.update_attribute(:sunk, sunk)}
        return
      end
    end
  end

  def self.failed_nuke(x,y,status)
    logger.info "\033[0;31mFailed nuke - x: #{x}, y: #{y}\033[0;37m"
    create(:x => x, :y => y, :hit => false, :status => status)
  end
  
  def self.successful_nuke(x,y,status,sunk=nil)
    logger.info "\033[0;32mSuccessful nuke - x: #{x}, y: #{y}\033[0;37m"
    logger.info "\033[0;32mSunk one - #{sunk}\033[0;37m" if sunk
    create(:x => x, :y => y, :hit => true, :status => status, :sunk => sunk)
  end
  
  def self.coords_for_grid(num_x, num_y)
    coords = []
    num_x.times do |x|
      num_y.times do |y|
        coords << [x,y]
      end
    end
    coords
  end
end
