require 'net/http'
require 'uri'
class Game < ActiveRecord::Base
  attr_accessible :game_status, :p45_game_id, :name, :email
  
  validates :name, :email, :presence => true

  has_many :ships
  has_many :salvos

  after_create :create_fleet # add the opponent's (p45's) ships for tracking
  after_create :register_game # register the game with p45

  def sink_ship(name)
    ship_to_sink = ships.alive.where(:name => name).first
    ship_to_sink.sink! if ship_to_sink
  end

  def nuke(x,y)
    params = {:id => p45_game_id, :x => x.to_i, :y => y.to_i}
    #resp = Net::HTTP.post_form(URI.parse(battle_host + "nuke"), params)
    resp = post_to_battle_host("/nuke", params)
    if resp.code.to_i == 200
      data = JSON.parse(resp.body)
      salvos.fire(x,y,data["status"],data["sunk"]) if data["status"]
      salvos.received_enemy_fire(data["x"], data["y"]) if data["x"] and data["y"] 
      # update game status when needed
      update_attribute(:game_status, data["game_status"]) if data["game_status"]
      update_attribute(:game_status, data["prize"]) if data["prize"]
    else
      raise "Error nuking: #{resp.body}"
    end
  end

  # play the move
  def play_move(coords={})
    unless game_status
      if x = coords[:x] and y = coords[:y]
        nuke(x,y)
      else
        nuke(*salvos.next_best_shot)
      end
    end
  end

  def game_over?
    !game_status.blank?
  end
  
  private 

  def register_game
    params = {:name => name, :email => email}
    #resp = Net::HTTP.post_form(URI.parse(battle_host + "register"), params.to_json)
    resp = post_to_battle_host("/register", params)
    if resp.code.to_i == 200
      data = JSON.parse(resp.body)
      logger.debug "Registered game ... data: #{data.inspect}"
      logger.info "Registered game successfully: #{data["id"]}" 
      update_attribute(:p45_game_id, data["id"])
      salvos.received_enemy_fire(data["x"], data["y"]) if data["x"] and data["y"]
    else
      raise "Error registering game: #{resp.body}"
    end
  end

  def post_to_battle_host(path, params)
    req = Net::HTTP::Post.new(path) #, initheader = {'Content-Type' =>'application/json'})
    req.body = params.to_json
    resp = Net::HTTP.new(battle_host).start{|http| http.request(req)}
  end

  def battle_host
    "battle.platform45.com"
  end

  # generate the 7 ships to track p45's fleet
  def create_fleet
    # name => length
    [{"Carrier"     => 5}, {"Battleship" => 4}, {"Destroyer"   => 3},
     {"Submarine"   => 2}, {"Submarine"  => 2}, {"Patrol Boat" => 1},
     {"Patrol Boat" => 1}].each do |ship_info|
      ships.create(:name => ship_info.keys.first, :length => ship_info.values.first)
    end
  end
end
