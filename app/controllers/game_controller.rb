class GameController < ApplicationController

  def index
    if params[:game_over]
      game = Game.find(params[:game_over])
      flash.now[:notice] = "You Won ... congrats!" if game.game_status != "lost"
      flash.now[:notice] = "You Lost ... sorry :(" if game.game_status == "lost"
    end
    @games = Game.all
  end

  def new
    @game = Game.new
  end

  def play
    if params[:id]
      @game = Game.find(params[:id])
    else
      @game = Game.create(params[:game])
    end
    @play_manually = params[:manually] == "false" ? false : true # default to manually
    @game.play_move unless @play_manually
  end

  def update_grid
    @game = Game.find(params[:id])
    @game.play_move(:x => params[:x], :y => params[:y])
  end
end
