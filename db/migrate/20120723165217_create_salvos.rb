class CreateSalvos < ActiveRecord::Migration
  def change
    create_table :salvos do |t|
      t.integer :x
      t.integer :y
      t.string :status
      t.integer :game_id
      t.string :error
      t.boolean :enemy, :default => false
      t.boolean :hit, :default => false
      t.string :sunk

      t.timestamps
    end
  end
end
