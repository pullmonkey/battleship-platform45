class CreateShips < ActiveRecord::Migration
  def change
    create_table :ships do |t|
      t.string :name
      t.integer :game_id
      t.boolean :sunk, :default => false
      t.integer :length

      t.timestamps
    end
  end
end
