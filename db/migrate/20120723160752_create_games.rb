class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :p45_game_id
      t.string :game_status
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
