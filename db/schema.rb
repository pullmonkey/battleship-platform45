# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120723165217) do

  create_table "games", :force => true do |t|
    t.string   "p45_game_id"
    t.string   "game_status"
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "salvos", :force => true do |t|
    t.integer  "x"
    t.integer  "y"
    t.string   "status"
    t.integer  "game_id"
    t.string   "error"
    t.boolean  "enemy",      :default => false
    t.boolean  "hit",        :default => false
    t.string   "sunk"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "ships", :force => true do |t|
    t.string   "name"
    t.integer  "game_id"
    t.boolean  "sunk",       :default => false
    t.integer  "length"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

end
